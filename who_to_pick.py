from bs4 import BeautifulSoup
import urllib
import re
import predict_fns as f1
import sys
import pandas as pd


code = {}
code['CANADA'] = 985
code['FRANCE'] = 986
code['AUSTRIA'] = 987
code['GREAT-BRITAIN'] = 988
code['GERMANY'] = 989
code['HUNGARY'] = 990
code['BELGIUM'] = 991
code['ITALY'] = 992
code['SINGAPORE'] = 993
code['RUSSIA'] = 994
code['JAPAN'] = 995
code['UNITED-STATES'] = 996
code['MEXICO'] = 997
code['BRAZIL'] = 998
code['ABU-DHABI'] = 999



gp = sys.argv[1]
pract = sys.argv[2]

pr_string = 'https://www.formula1.com/en/results.html/2018/races/'+str(code[gp.upper()])         +'/'+gp.lower()+'/practice-'+str(pract)+'.html'
soup = BeautifulSoup(urllib.request.urlopen(pr_string))
S = soup.get_text()

soup_con = BeautifulSoup(urllib.request.urlopen('https://www.formula1.com/en/results.html/2018/drivers.html'))
C = soup_con.get_text()


lst = []
for driver in ['HAM','VET','VER','RIC','RAI','BOT','GRO','OCO','PER','ALO','LEC','HAR','MAG','HUL','ERI','VAN','SAI','GAS','STR','SIR']:
    print('----------------------------------')
    try:
        vt = [m.start() for m in re.finditer(driver, S)][-1]
        x = S[0:vt]
        n_vec = [m.start() for m in re.finditer('\n', x)]
        P2pos = S[n_vec[-6]+1:n_vec[-5]]
        vtc = [m.start() for m in re.finditer(driver, C)][-1]
        xc = C[0:vtc]
        n_vecc = [m.start() for m in re.finditer('\n', xc)]
        posc = C[n_vecc[-6]+1:n_vecc[-5]]
        print(driver)
        print('Practice    ' + P2pos)
        print('Driver      ' + posc)
        p,MAP,cmap,c3 = f1.predict(int(P2pos),int(posc),0,session='PRACTICE '+str(pract))
        lst.append((driver,[int(P2pos),int(posc),MAP,cmap,c3]))
    except Exception as e: print(e)
X = pd.DataFrame.from_dict(dict(lst),orient='index', columns=['P'+str(pract),'Dri','MAP','p(MAP)','p(>3)'])
X = X.round(decimals = 2)
print('\n\n\n\n')
print(gp.upper()+' GRAND PRIX, PRACTICE '+str(pract)+', RANK BY MAP')
print('\n')
print(X.sort_values(by=['MAP'],ascending=[True]))

print('\n\n\n\n')
print(gp.upper()+' GRAND PRIX, PRACTICE '+str(pract)+', RANK BY PODIUM PROBABILITY')
print('\n')
print(X.sort_values(by=['p(>3)'],ascending=[False]))
