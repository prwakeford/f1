library(DBI)
library(RSQLite)
setwd("~/Documents/repositories/wranglingf1datawithr/src/")
f1 =dbConnect(RSQLite::SQLite(), './/f1com_results_archive.sqlite')

prResults=dbGetQuery(f1,'SELECT * FROM pResults WHERE year>="2010" AND session="PRACTICE 1"')
quResults=dbGetQuery(f1,'SELECT * FROM qualiResults WHERE year>="2010"')
raResults=dbGetQuery(f1,'SELECT * FROM raceResults WHERE year>="2010"')

practice_and_race=merge(prResults,raResults,by.x=c("driverName","year","race"),by.y=c("driverName","year","race"))
int_res=as.integer(practice_and_race$pos.y)
int_res[is.na(int_res)]=100
#D=hist(int_res[practice_and_race$pos.x==1],breaks=1:101)
#plot(D$breaks[1:25],D$counts[1:25])

#mean(int_res[practice_and_race$pos.x==1])
#sd(int_res[practice_and_race$pos.x==1])

#D=hist(int_res[practice_and_race$pos.x==15],breaks=1:101,plot=FALSE)
#plot(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1))
#lines(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1))



for (i in 1:4)  
  D=hist(int_res[practice_and_race$pos.x==i],breaks=1:101,plot=FALSE)
  plot(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1))
  lines(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1))
  par(new=TRUE)
  
  
proba <- c()
#jpeg('rplot.jpg')  
#get( getOption( "device" ) )()
par( mfrow = c( 5, 5 ) )
par(mar = c(2,2,2,2))

for (i in 1:24){
  
  D=hist(int_res[practice_and_race$pos.x==i],breaks=1:101,plot=FALSE)
  cs=cumsum(D$counts[1:25])/sum(D$counts[1:25])
  proba <- append(proba,sum(cs[3]))
  plot(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1),xlab="x",ylab="y",main=paste("P1 finish ",toString(i)))
  lines(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1))
  grid(10)
  
  }
dev.off()



#jpeg('rplot.jpg')
#get( getOption( "device" ) )()
par( mfrow = c( 5, 5 ) )
par(mar = c(2,2,2,2))

for (i in 1:24){
  D=hist(int_res[practice_and_race$pos.x==i],breaks=1:101,plot=TRUE)

  plot(D$breaks[1:25],D$counts[1:25],,xlab="x",ylab="y",main=paste("P1 finish ",toString(i)))
  lines(D$breaks[1:25],D$counts[1:25])
  
  
}
dev.copy(png,'myplot.png')
dev.off()

fulltable <- matrix(NA,25,24)
for (i in 1:24){
  D=hist(int_res[practice_and_race$pos.x==i],breaks=1:101,plot=FALSE)
  fulltable[,i]=c(D$counts[1:25]/sum(D$counts[1:25]))
  
  write.csv(c(D$counts[1:25]/sum(D$counts[1:25])), file="ptable.txt",append = TRUE)}




D=hist(int_res[practice_and_race$pos.x==1],breaks=1:101,plot=FALSE)
plot(D$breaks[1:25],D$counts[1:25],xlab="x",ylab="y",main=paste("P1 finish ",toString(i)))
lines(D$breaks[1:25],D$counts[1:25])



D=hist(int_res[practice_and_race$pos.x==1],breaks=1:101,plot=FALSE)
plot(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1),xlab="x",ylab="y",main=paste("P1 finish ",toString(i)))
lines(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1))
grid(10)







proba <- c()
#jpeg('rplot.jpg')  
#get( getOption( "device" ) )()
par( mfrow = c( 5, 5 ) )
par(mar = c(2,2,2,2))

for (i in 1:24){
  
#  get( getOption( "device" ) )()
#  jpeg(paste("P1 finish ",toString(i)),width=1000,height=500) 
  par( mfrow = c( 1, 2 ) )
  par(mar = c(2,2,2,2))
  
  D=hist(int_res[practice_and_race$pos.x==i],breaks=1:101,plot=FALSE)
  plot(D$breaks[1:25],D$counts[1:25],xlab="x",ylab="y",xlim=c(1,25),main=paste("P1 finish ",toString(i)),asp=2)
  lines(D$breaks[1:25],D$counts[1:25],xlim=c(1,25))
  grid(25)

  plot(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1),xlab="x",ylab="y",main=paste("P1 finish ",toString(i)))
  lines(D$breaks[1:25],cumsum(D$counts[1:25])/sum(D$counts[1:25]),ylim=c(0,1))
  grid(25)
  dev.off()
}




