function [diff]=find_best_fit(data)


muvec=linspace(1,20,50);
sigvec=linspace(0.5,20,50);
ampvec=linspace(0.75,1.25,10);

for mu=1:length(muvec)
    for sig=1:length(sigvec)
        for amp=1:length(ampvec)
            pd = makedist('Normal','mu',muvec(mu),'sigma',sigvec(sig));
            t = truncate(pd,1,24);
            x=1:24;
            p=pdf(t,x)*ampvec(amp);
            
            diff(mu,sig,amp)=sqrt(abs(sum(p.^2-data.^2)));
            
            %figure(1)
            %clf
            %plot(data)
            
            %hold on
            %plot(p);drawnow

        end
    end
end
        
%imagesc(muvec,sigvec,sum(diff,3))