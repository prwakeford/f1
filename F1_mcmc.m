function [news]=F1_mcmc(data,olds,sigs,low_range,high_range,N)

n_dim=max(size(sigs));
news=zeros(N,n_dim);
old = -inf;
flag = 0;

% Nest
for i=1:N

while (~flag)

out = 1;
while (out)
    
% Throw a point in 
point=olds+sigs.*randn(1,n_dim);

% Check it is in range
if (any(point>high_range) || any(point<low_range))
out = 1;
else
out = 0;
end
end


pd = makedist('Normal','mu',point(1),'sigma',point(2));

t = truncate(pd,1,24);
x=1:24;

test=-sum(sqrt(data.^2-pdf(t,x)*point(3).^2));

% Accept point, or not
a = log(rand);
if (test>old)
flag = 1;
elseif (a<(test - old))
flag = 1;
end

end


% Add the accepted point to list
news(i,:)=point;
%sprintf('New point chosen at [ %i, %i, %i]',point(1),point(2),point(3))


% Update
olds=point;
old = test;
flag = 0;

end