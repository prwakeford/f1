import numpy as np
from scipy.stats import kendalltau
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import sqlite3
import pandas as pd

from scipy import stats

class Driver:
    def __init__(self,name):
        self.name=name

    def get_performance(self):
        con = sqlite3.connect(r'./src/f1com_results_archive.sqlite')

        st='driverName="'+self.name+'"'
        self.performance = pd.read_sql('SELECT * FROM raceResults WHERE '+st, con)

def truncated_dist(mu,sigma,lower,upper):
    X = stats.truncnorm((lower - mu) / sigma, (upper - mu) / sigma, loc=mu, scale=sigma)
    return X

def p1_prior(p1):
    X=truncated_dist(p1*1.2,4,1,24)
    offsets=np.linspace(0.02,0,24)
    print(p1-1)
    print(len(offsets))
    return X.pdf(range(1,24))+offsets[p1-1]

def driver_prior(d):
    Y=truncated_dist(d,3,1,24)
    return Y.pdf(range(1,24))


def constructor_prior(c):
    Z=truncated_dist(c,3,1,24)
    return Z.pdf(range(1,24))


def plot_dist(p1,p2,p3):
    plt.plot(range(1,24),p1*p2)
    plt.grid()
    plt.xlim([1,24])
    plt.show()



def get_final_results(p1, session='PRACTICE 2',race_NAME = "nall"):

    con = sqlite3.connect(r'./src/f1com_results_archive.sqlite')

    if race_NAME == "nall":
        race = pd.read_sql('SELECT * FROM raceResults', con)
        pra = pd.read_sql('SELECT * FROM pResults', con)

    else:
        print(race_NAME)
        sR = 'SELECT * FROM raceResults where race ="'+race_NAME+'"'
        sP = 'SELECT * FROM pResults where race ="'+race_NAME+'"'
        race = pd.read_sql(sR , con)
        pra = pd.read_sql(sP , con)


    D=race.merge(pra, how='right',on=['year','race','driverName'])
    p=D[(D['session_y']==session) & (D['pos_y']==p1)].pos_x


    P=pd.to_numeric(p, errors='coerce')
    P=P.dropna().as_matrix()

    return P



def sample_from_P2_results2(p1pos,session='PRACTICE 2',race='nall',plott=0):



    p=get_final_results(p1pos, session,race_NAME=race)


    med=stats.mode(p)[0]
    st=p.std()

    if plott:


        X=truncated_dist(med,st,1,24)
        plt.plot(range(1,24),X.pdf(range(1,24)))
        plt.show()


    return med,st


def get_posterior(p1=0,dri=0,con=0,session='PRACTICE 2',race='nall',plott=0):

    p1_prior=1
    dri_prior=1
    con_prior=1

    if p1 != 0:

        p1_med,p1_std=sample_from_P2_results2(p1,session,race)
        X=truncated_dist(p1_med,p1_std,1,24)
        p1_prior=X.pdf(range(1,24))

    if dri != 0:
        dri_prior=driver_prior(dri)

    if con != 0:
        con_prior=constructor_prior(con)

    posterior=p1_prior*dri_prior*con_prior

    if plott:
        plt.subplot(2, 1, 1)
        plt.plot(range(1,24),posterior,'c')
        plt.plot(range(1,24),posterior,'r^')
        plt.grid(b=True, which='major', color='k', linestyle='-')
        plt.title('Posterior')
        ax = plt.gca()
        ax.set_facecolor('white')

        plt.subplot(2, 1, 2)
        plt.title('CDF')
        plt.plot(range(1,24),np.cumsum(posterior)/np.sum(posterior),'c')
        plt.plot(range(1,24),np.cumsum(posterior)/np.sum(posterior),'r^')
        plt.grid(b=True, which='major', color='k', linestyle='-')
        ax = plt.gca()
        ax.set_facecolor('white')
        plt.show()


    return posterior


def pos_cumsum(position,p1=0,dri=0,con=0,session='PRACTICE 2',plott=0):
    post=get_posterior(p1,dri,con,session,plott=0)
    cumulative_sum=np.cumsum(post)/np.sum(post)
    cumpos=cumulative_sum[position-1]
    print('Prob better than '+str(position)+' = '+str(cumpos))
    return cumpos



def predict(p1,dri,con,session='PRACTICE 2',race='nall',plott=0):
	# Practice, Dri, Con, Session, Plotflag
	p=get_posterior(p1,dri,con,session,race,plott)
	x=np.linspace(1,23,23)
	MAP=np.where(p==np.amax(p))[0]+1
	print('MAP estimate - '+str(MAP[0]))
	cmap=pos_cumsum(MAP[0],p1,dri,con,session)
	c3=pos_cumsum(3,p1,dri,con,session)
	return p,MAP[0],cmap,c3
