from bs4 import BeautifulSoup
import urllib
import re
import predict_fns as f1
import sys


code = {}
code['CANADA'] = 985
code['FRANCE'] = 986
code['AUSTRIA'] = 987
code['GREAT-BRITAIN'] = 988
code['GERMANY'] = 989
code['HUNGARY'] = 990
code['BELGIUM'] = 991
code['ITALY'] = 992
code['SINGAPORE'] = 993
code['RUSSIA'] = 994
code['JAPAN'] = 995
code['UNITED-STATES'] = 996
code['MEXICO'] = 997
code['BRAZIL'] = 998
code['ABU-DHABI'] = 999



gp = sys.argv[0]
pract = sys.argv[1]

pr_string = 'https://www.formula1.com/en/results.html/2018/races/'+str(code[gp.upper()])         +'/'+gp.lower()+'/practice-'+str(pract)+'.html'

#soup = BeautifulSoup(urllib2.urlopen('https://www.formula1.com/en/results.html/2018/races/985/canada/practice-2.html'))
soup = BeautifulSoup(urllib.request.urlopen(pr_string))

S = soup.get_text()

#oup_con = BeautifulSoup(urllib2.urlopen('https://www.formula1.com/en/results.html/2018/drivers.html'))
soup_con = BeautifulSoup(urllib.request.urlopen('https://www.formula1.com/en/results.html/2018/drivers.html'))
C = soup_con.get_text()
#S[[m.start() for m in re.finditer('PRACTICE', S)][1]:-1]


#for driver in ['HAM','VET','VER','RIC','RAI','BOT']:
#    vt = [m.start() for m in re.finditer(driver, S)][-1]
#    print(S[vt-30:vt+30])
#    print(driver)
#    for i in range(20,100):
#        if S[vt - i].isdigit():
#            if S[vt - i-1].isdigit():
#                pos = int(S[vt-i-1:vt-i])
#            else
#                pos = int(S[vt-i])
#		break
#    print(pos)



for driver in ['HAM','VET','VER','RIC','RAI','BOT','GRO','OCO','PER','ALO','LEC','HAR','MAG','HUL','ERI','VAN','SAI','GAS','STR','SIR']:
    print('----------------------------------')
    vt = [m.start() for m in re.finditer(driver, S)][-1]
    x = S[0:vt]
    n_vec = [m.start() for m in re.finditer('\n', x)]
    P2pos = S[n_vec[-6]+1:n_vec[-5]]
    vtc = [m.start() for m in re.finditer(driver, C)][-1]
    xc = C[0:vtc]
    n_vecc = [m.start() for m in re.finditer('\n', xc)]
    posc = C[n_vecc[-6]+1:n_vecc[-5]]
    print(driver)
    print('Practice    ' + P2pos)
    print('Constructor ' + posc)
    p = f1.predict(int(P2pos),int(posc),0)


#for driver in ['HAM','VET','VER','RIC','RAI','BOT','GRO','OCO','PER','ALO','LEC','HAR','MAG','HUL','ERI','VAN','SAI','GAS','STR','SIR']:
#    print(driver)
#    vtc = [m.start() for m in re.finditer(driver, C)][-1]
#    xc = C[0:vt]
#    n_vecc = [m.start() for m in re.finditer('\n', x)]
#    posc = C[n_vec[-6]+1:n_vec[-5]]
#    print(pos)
